﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	Rigidbody rb;
	public int speed;
	public int rotateSpeed;

	void Start(){

		rb = GetComponent <Rigidbody> ();
	}

	void FixedUpdate(){

		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		transform.Rotate (Vector3.up, h * rotateSpeed * Time.deltaTime);
		rb.AddForce (transform.forward*speed * v);

	}
}
