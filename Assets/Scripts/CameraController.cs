﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

	public GameObject endCamera;
	public GameObject playerCam;
	public GameObject cameraLook;
	public GameObject player;


	public float speed;

	Vector3 offset;


	void Awake ()
	{
		offset = endCamera.transform.position - player.transform.position; 
		endCamera.SetActive (false);
		cameraLook.SetActive (false);
		
	}

	private float camActiveTime;
	private float lookTime = 10f;

	void LateUpdate ()
	{
	
		if (player == null) {

			playerCam.SetActive (false);
			endCamera.SetActive (true);
			CameraAni ();
			return;
		}
		if (Input.GetKeyDown (KeyCode.R)) {

			camActiveTime = Time.time;
			playerCam.SetActive (false);
			cameraLook.SetActive (true);

		}
		if (Time.time > camActiveTime + lookTime) {
			playerCam.SetActive (true);
			cameraLook.SetActive (false);
		}

		endCamera.transform.position = player.transform.position + offset;

	}

	void CameraAni ()
	{

		Vector3 lastCamPos = new Vector3 (6.0f, 13.0f, 20f);
		Vector3 lastCamRot = new Vector3 (45, 180, 0.0f);
		Vector3 endCameraCurRot = endCamera.transform.eulerAngles;

		endCamera.transform.position = Vector3.Lerp (endCamera.transform.position, lastCamPos, speed * Time.deltaTime);

		endCameraCurRot = new Vector3 (Mathf.Lerp (endCameraCurRot.x, lastCamRot.x, Time.deltaTime),
			Mathf.Lerp (endCameraCurRot.y, lastCamRot.y, Time.deltaTime),
			Mathf.Lerp (endCameraCurRot.z, lastCamRot.z, Time.deltaTime));

		endCamera.transform.eulerAngles = endCameraCurRot;

	}

}
