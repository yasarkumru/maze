# **özBlizzard Maze** #

#**Kullanılacak teknolojiler**
Projemizi geliştirirken kullanacağımız programlama dili C#. Projeyi Unity3D oyun motoru ile geliştireceğiz. Unity C# ve JavaScripti destekliyor olsa da C# ile geliştirme kararı aldık. Unity3D çapraz platform bir motor olup, projemizi PC, Mac, Android ve iOS için derlememize olanak vermektedir. 

# **Sistem gereksinimleri** #
Oyunun derlenmesinde jdk 1.8 ve android sdk 25.1 kullanacağız, fakat minimum Android 2.3.1, Mac için minimum 10.8 versiyonu (OpenGL 3.2), Linux ve Windows için ise yine OpenGL 3.2 versiyonu minimum yeterli olacak. Şu an bir iOS cihazımız olmadığından onun için derlemeyeceğiz. Her ne kadar Unity Windows için DirectX desteği sağlıyor olsa da projeyi Linux üzerinde geliştirdiğimiz için şimdilik böyle bir destek sağlamayacağız. Bunun yanında kullanıcının konuşmalarını almak için bir mikrofon yeterli olacaktır. 
Ses algılanması için kullanacağımız api’ye tam olarak karar vermedik fakat bunun Google’ın Speech To Text apisi olması muhtemel. Bundan dolayıda oyunun çalışacağı bilgisayar veya telefonda internet bağlantısı şart olabilir.

Demo video https://www.youtube.com/watch?v=wtSsO3Y92IA 

# Sınıf diyagramı #

![Diagram1.png](https://bitbucket.org/repo/RBknEy/images/2574072625-Diagram1.png)

* İlker Çatak 081101007
* Yaşar Kumru 081101001
* ibrahim Topaloğlu 091101034